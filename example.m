filename = "samples/test_P01_1-2.wav";

[sample, fr] = audioread(filename);
tic;
[left_ch, right_ch] = extractTaps(sample, fr);
toc

disp("Number of taps detected:")
disp("Left channel:")
disp(length(left_ch))
disp("Right channel:")
disp(length(right_ch))

