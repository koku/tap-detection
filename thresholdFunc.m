function [threshold] = thresholdFunc(signal, frate, varargin)
%THRESHOLDFUNC Function to compute the threshold based on the signal
%   param signal: the input signal (in debibels)
%   param frate: the sampling rate of the signal (default 44100)
%   param c: scaling coeff for the mean (default 0.15)
%   param minbound: specifying minimum value of the threshold (default
%                   -inf)
%   param wintime: duration of the window for the mean calculation (default
%                  100 ms)

narginchk(1, 5);
if nargin < 2
    frate = 44100;
end

if nargin == 5
    winsize = floor(frate * varargin{3} / 1000) - 1;
else
    winsize = frate / 10 - 1; % 100 ms window
end
if nargin == 4
    minbound = varargin{2};
else
    minbound = -inf;
end
if nargin == 3
    c = varargin{1};
else
    c = 0.20;
end

prefilter = movmean(signal, [winsize 0]);
prefilter(1:(winsize + 1)) = prefilter(winsize + 2);
threshold = max((1 - c) * prefilter, minbound);
end

