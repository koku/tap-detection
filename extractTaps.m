function varargout = extractTaps(sample, frate, c)
%EXTRACTTAPS Extracts the taps of an audio sequence
%   param sample: audio sample (can be multi channel)
%   param framerate: framerate (default 44100)
%   param c: scaling coeffincient of the mean 
%   returns: vector of taps for each channel of the audio file

% Arguments handeling
narginchk(1, 3);
[~, col] = size(sample);
varargout = cell(1, col);
nargoutchk(col, col);

if nargin < 2
    frate = 44100;
end

if nargin < 3
    c = 0.20;
end


wlength = 10; % 10 ms window size

% Creating one output per column of the sample input (channels)
for i=1:col
    % Actual algorithm
    decibels = energy(sample(:, i), wlength);

    % threshold function
    threshold = thresholdFunc(decibels, frate, c, -35);
    % We substract 1 so the index starts at 0 so that the timing is right
    timing = (find(decibels >= threshold) - 1) / frate;
    % Two taps are spaced by at least 50ms
    attacks = flipud(abs(diff(flipud(timing))) > 0.05);
    % we always need to take the first detected tap 
    attacks = [true; attacks];

    varargout{i} = timing(attacks);
end

end