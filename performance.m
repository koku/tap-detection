filename = "test_P01_01.wav";
info = audioinfo(filename);


[sample, fr] = audioread(filename);

tic;
[taps_user, taps_ref] = extractTaps(sample, fr);
toc

tic;
peaks_user = peakDetection(sample(:, 1), fr);
peaks_ref = peakDetection(sample(:, 2), fr);
toc

if all(size(peaks_user) == size(taps_user))
    [muser, iuser] = max(abs(peaks_user - taps_user));
    disp("Index  Maximum Error:")
    disp([iuser, muser])
    peaks_user(iuser)
    taps_user(iuser)
end

if all(size(peaks_ref) == size(taps_ref))
    [mref, iref] = max(abs(peaks_ref - taps_ref));
    disp([iref, mref])
    peaks_ref(iref)
    taps_ref(iref)
end