function pow = energy(input, wlength)
%ENERGY Calculate the normalized energy of a signal in db 
%   param input: input signal
%   param wlength: window length (default 10ms)
if nargin < 2
    wsize = floor(44100 * 10 / 1000) - 1;
else
    wsize = floor(44100 * wlength / 1000) - 1;
end
prefilter = movmean(input .^ 2, [wsize 0]);
prefilter(1:(wsize + 1)) = prefilter(wsize + 2);
pow = 10 * log10(2 * prefilter);
end

