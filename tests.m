clear;
% Read tap and set seed
[sample, fr] = audioread("tap.wav");
NTAPS = 1;
REFTAPS = 1;
% [sample, fr] = audioread("tap_sequence.wav");
% NTAPS = 117;
rng(130);
N = length(sample);
time = (0:(N - 1)) / fr;

% No noise test
disp("No noise")
taps = extractTaps(sample, fr);

pow = energy(sample);
thrsh = thresholdFunc(pow, fr);
plot(time, [pow thrsh]);
plotTaps(taps);
legend('Signal', 'Threshold', 'Tap');
% Test on detection
assert(length(taps) >= NTAPS, "Not enough taps detected");
assert(length(taps) <= NTAPS, "Too much taps detected");

pause()

for amplitude=0.1:0.1:0.5
    for color=["white", "pink", "brown"]
        disp("Noise Type =")
        disp([color amplitude])
        % adding noise
        noised = sample + amplitude * coloredNoise(N, color);
        % tap extraction
        taps = extractTaps(noised, fr);
        % plot
        pow = energy(noised);
        thrsh = thresholdFunc(pow, fr);
        plot(time, [pow thrsh]);
        plotTaps(taps);
        legend('Signal', 'Threshold', 'Tap');
        % Test on detection
        assert(length(taps) >= NTAPS, "Not enough taps detected");
        assert(length(taps) <= NTAPS, "Too much taps detected");
        % Test on timing
        disp("Absolute timing error:")
        disp(abs(taps - REFTAPS))
        % Test on millisecond precision
        % assert(all(abs(tap - REFTAPS) < 10 ^ -3), "Error greater than one ms");
        pause()
    end
end

function plotTaps(taps)
    for tap=taps' 
        line([tap tap], ylim, 'Color', [0.4660 0.6740 0.1880], 'LineWidth', 1.5);
    end
end