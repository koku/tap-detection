# Audio sample Tap detection Algorithm

Matlab algorithm to detect tap in audio samples. Two methods are used:

- Using the zscore algorithm
- Using moving norm and filter values above the 95% quantile
