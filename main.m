% Visualize tap detection for an audio file
clear;
[sample, fr] = audioread("tap_sequence.wav");
NTAPS = 117;

% X axis
N = length(sample);
time = (0:(N - 1)) / fr;

taps = extractTaps(sample, fr);
% ploting the algorithm
pow = energy(sample);
thrsh = thresholdFunc(pow, fr, 0.20, -35);

plot(time, [pow thrsh]);
plotTaps(taps);
legend('Signal', 'Threshold', 'Tap');

function plotTaps(taps)
    for tap=taps' 
        line([tap tap], ylim, 'Color', [0.4660 0.6740 0.1880]);
    end
end