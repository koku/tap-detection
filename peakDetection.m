function [peaks] = peakDetection(signal, frate)
%PEAKDETECTION Detect the peaks of the signal
%   :param signal: input signal to detect the peaks
%   :param delay: number of values to take into accounts in the process
%   :param threshold: detection threshold
%   :param influence: determine the influence of the previous signal
%                     0 for no influence,
%                     1 for maximum influence.
%
%   :returns: Indices of the detections
%
%   Credit:
%   Jean-Paul van Brakel
%   https://stackoverflow.com/questions/22583391/peak-signal-detection-in-realtime-timeseries-data/22640362#22640362
zsignal = zscore(signal);
timing = (find(zsignal > quantile(zsignal, 0.96)) - 1) / frate;
attacks = [true; flipud(abs(diff(flipud(timing))) > 0.05)];
peaks = timing(attacks);

end

