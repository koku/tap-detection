clear;
% setup
[sample, fr] = audioread("tap_piano.wav");
N = length(sample);
time = (0:(N-1)) / fr;
% add noise
A = 0;
processed = max(min(sample + A .* coloredNoise(N), 1), -1);
% calculate db
db = energy(processed);
% threshold function
c = 0.15;
winsize = fr / 10; % 100 ms window
threshold = max((1 - c) * movmean(db, [winsize 0]), -inf);
% plot
plot(time, [db, threshold]);
legend("Sample (db)", "Threshold");
xlabel("Time (s)");
ylabel("Amplitude (db)");