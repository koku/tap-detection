function noise = coloredNoise(n, color)
%COLOREDNOISE Generate colored noise within [-1 1]
%   param n: length of signal
%   param color: color of the noise (default white)
%                valid colors : white, pink, or brown
narginchk(1, 2);
if nargin < 2
    color = "white";
end

% param checking
validateattributes(n, {'double'}, {'integer', 'scalar'});
color = validatestring(color, ["white", "pink", "brown"]);

% white noise centered around 0 with amplitude 1
w = 2 * rand([n 1]) - 1;
switch color
    case "white"
        noise = w;
    case "pink"
        noise = w;
    case "brown"
        noise = w;
end

end